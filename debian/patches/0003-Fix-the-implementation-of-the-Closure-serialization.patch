From: William Desportes <williamdes@wdes.fr>
Date: Wed, 25 Dec 2024 13:27:37 +0100
Subject: Fix the implementation of the Closure serialization

The patches where found on a fork of upstream:
- https://github.com/charescape/serialize-closure/pull/12
- https://github.com/charescape/serialize-closure/commit/963613949e48e5d6e49255a4a55b7d109db54de0

Origin: vendor
Forwarded: no
---
 src/ReflectionClosure.php         |  4 ++
 src/SerializableClosure.php       | 95 +++++++++++++++++++--------------------
 tests/ClosureTest.php             | 34 ++++++++------
 tests/JsonSerializableClosure.php | 32 +++++++++----
 tests/SignedClosureTest.php       | 60 ++++++++++++++++++++++---
 5 files changed, 148 insertions(+), 77 deletions(-)

diff --git a/src/ReflectionClosure.php b/src/ReflectionClosure.php
index 196f296..3c48ed7 100644
--- a/src/ReflectionClosure.php
+++ b/src/ReflectionClosure.php
@@ -87,6 +87,10 @@ class ReflectionClosure extends ReflectionFunction
             $className = '\\' . trim($className->getName(), '\\');
         }
 
+        if ($className === null) {
+            $className = '';
+        }
+
         $builtin_types = self::getBuiltinTypes();
         $class_keywords = ['self', 'static', 'parent'];
 
diff --git a/src/SerializableClosure.php b/src/SerializableClosure.php
index 1025ff5..57e9f90 100644
--- a/src/SerializableClosure.php
+++ b/src/SerializableClosure.php
@@ -110,11 +110,9 @@ class SerializableClosure implements Serializable
     }
 
     /**
-     * Implementation of Serializable::serialize()
-     *
-     * @return  string  The serialized closure
+     * Implementation of Serializable::__serialize()
      */
-    public function serialize()
+    public function __serialize()
     {
         if ($this->scope === null) {
             $this->scope = new ClosureScope();
@@ -147,17 +145,17 @@ class SerializableClosure implements Serializable
 
         $this->mapByReference($use);
 
-        $ret = \serialize(array(
+        $ret = array(
             'use' => $use,
             'function' => $code,
             'scope' => $scope,
             'this' => $object,
             'self' => $this->reference,
-        ));
+        );
 
-        if (static::$securityProvider !== null) {
-            $data = static::$securityProvider->sign($ret);
-            $ret =  '@' . $data['hash'] . '.' . $data['closure'];
+        if (static::$securityProvider !== null && $this->scope->serializations === 1) {
+            $ser = \serialize($ret);
+            $ret = static::$securityProvider->sign($ser);
         }
 
         if (!--$this->scope->serializations && !--$this->scope->toserialize) {
@@ -167,6 +165,16 @@ class SerializableClosure implements Serializable
         return $ret;
     }
 
+    /**
+     * Implementation of Serializable::serialize()
+     *
+     * @return  string  The serialized closure
+     */
+    public function serialize()
+    {
+        return \serialize($this->__serialize());
+    }
+
     /**
      * Transform the use variables before serialization.
      *
@@ -179,69 +187,45 @@ class SerializableClosure implements Serializable
     }
 
     /**
-     * Implementation of Serializable::unserialize()
-     *
-     * @param   string $data Serialized data
-     * @throws SecurityException
+     * Implementation of Serializable::__unserialize()
      */
-    public function unserialize($data)
+    public function __unserialize($data)
     {
         ClosureStream::register();
 
         if (static::$securityProvider !== null) {
-            if ($data[0] !== '@') {
+            if (!isset($data['hash'])) {
                 throw new SecurityException("The serialized closure is not signed. ".
                     "Make sure you use a security provider for both serialization and unserialization.");
             }
 
-            if ($data[1] !== '{') {
-                $separator = strpos($data, '.');
-                if ($separator === false) {
-                    throw new SecurityException('Invalid signed closure');
-                }
-                $hash = substr($data, 1, $separator - 1);
-                $closure = substr($data, $separator + 1);
-
-                $data = ['hash' => $hash, 'closure' => $closure];
-
-                unset($hash, $closure);
-            } else {
-                $data = json_decode(substr($data, 1), true);
+            if (!isset($data['closure']) || !is_string($data['closure'])) {
+                throw new SecurityException('Invalid signed closure');
             }
 
-            if (!is_array($data) || !static::$securityProvider->verify($data)) {
+            if (!static::$securityProvider->verify($data)) {
                 throw new SecurityException("Your serialized closure might have been modified and it's unsafe to be unserialized. " .
                     "Make sure you use the same security provider, with the same settings, " .
                     "both for serialization and unserialization.");
             }
 
-            $data = $data['closure'];
-        } elseif ($data[0] === '@') {
-            if ($data[1] !== '{') {
-                $separator = strpos($data, '.');
-                if ($separator === false) {
-                    throw new SecurityException('Invalid signed closure');
-                }
-                $hash = substr($data, 1, $separator - 1);
-                $closure = substr($data, $separator + 1);
-
-                $data = ['hash' => $hash, 'closure' => $closure];
-
-                unset($hash, $closure);
-            } else {
-                $data = json_decode(substr($data, 1), true);
+            $securityProvider = static::$securityProvider;
+            static::$securityProvider = null;
+            $data = \unserialize($data['closure']);
+            static::$securityProvider = $securityProvider;
+        } elseif (isset($data['hash']) || isset($data['closure'])) {
+            if (!isset($data['closure']) || !isset($data['hash'])) {
+                throw new SecurityException('Invalid signed closure');
             }
 
-            if (!is_array($data) || !isset($data['closure']) || !isset($data['hash'])) {
+            if (!is_string($data['closure'])) {
                 throw new SecurityException('Invalid signed closure');
             }
 
-            $data = $data['closure'];
+            $data = \unserialize($data['closure']);
         }
 
-        $this->code = \unserialize($data);
-
-        // unset data
+        $this->code = $data;
         unset($data);
 
         $this->code['objects'] = array();
@@ -271,6 +255,17 @@ class SerializableClosure implements Serializable
         $this->code = $this->code['function'];
     }
 
+    /**
+     * Implementation of Serializable::unserialize()
+     *
+     * @param   string $data Serialized data
+     * @throws SecurityException
+     */
+    public function unserialize($data)
+    {
+        $this->__unserialize(\unserialize($data));
+    }
+
     /**
      * Resolve the use variables after unserialization.
      *
@@ -388,7 +383,7 @@ class SerializableClosure implements Serializable
             unset($value);
             unset($data[self::ARRAY_RECURSIVE_KEY]);
         } elseif($data instanceof \stdClass){
-            if(isset($storage[$data])){
+            if(isset($storage[$data]) && $storage[$data] instanceof \stdClass){
                 $data = $storage[$data];
                 return;
             }
diff --git a/tests/ClosureTest.php b/tests/ClosureTest.php
index 3b9baf0..8e7ae67 100644
--- a/tests/ClosureTest.php
+++ b/tests/ClosureTest.php
@@ -206,11 +206,15 @@ class ClosureTest extends \PHPUnit\Framework\TestCase
         $t2->func = $f;
 
         $t->subtest = $t2;
+        $this->assertSame($t->func, $t->subtest->func);
 
+        SerializableClosure::enterContext();
         $x = unserialize(serialize($t));
+        SerializableClosure::exitContext();
 
-        $g = $x->func;
-        $g = $g();
+        $this->assertSame($x->func, $x->subtest->func);
+        $fun = $x->func;
+        $g = $fun();
 
         $ok = $x->func == $x->subtest->func;
         $ok = $ok && ($x->subtest->func == $g);
@@ -358,27 +362,29 @@ class ObjnObj implements Serializable {
     public $subtest;
     public $func;
 
-    public function serialize() {
-
-        SerializableClosure::enterContext();
-
-        $object = serialize(array(
+    public function __serialize() {
+        $object = array(
             'subtest' => $this->subtest,
             'func' => SerializableClosure::from($this->func),
-        ));
-
-        SerializableClosure::exitContext();
+        );
 
         return $object;
     }
 
-    public function unserialize($data) {
-
-        $data = unserialize($data);
-
+    public function __unserialize($data) {
         $this->subtest = $data['subtest'];
         $this->func = $data['func']->getClosure();
     }
+
+    public function serialize()
+    {
+        return serialize($this->__serialize());
+    }
+
+    public function unserialize($data)
+    {
+        $this->__unserialize(\unserialize($data));
+    }
 }
 
 class A
diff --git a/tests/JsonSerializableClosure.php b/tests/JsonSerializableClosure.php
index 481ad5d..40ac30b 100644
--- a/tests/JsonSerializableClosure.php
+++ b/tests/JsonSerializableClosure.php
@@ -12,7 +12,7 @@ use Opis\Closure\SerializableClosure;
 
 class JsonSerializableClosure extends SerializableClosure
 {
-    public function serialize()
+    public function __serialize()
     {
         if ($this->scope === null) {
             $this->scope = new ClosureScope();
@@ -45,23 +45,39 @@ class JsonSerializableClosure extends SerializableClosure
 
         $this->mapByReference($use);
 
-        $ret = \serialize(array(
+        $ret = array(
             'use' => $use,
             'function' => $code,
             'scope' => $scope,
             'this' => $object,
             'self' => $this->reference,
-        ));
+        );
 
-        if (static::$securityProvider !== null) {
-            $data = static::$securityProvider->sign($ret);
-            $ret =  '@' . json_encode($data);
+        if (static::$securityProvider !== null && $this->scope->serializations === 1) {
+            $ser = \serialize($ret);
+            $ret = static::$securityProvider->sign($ser);
         }
 
         if (!--$this->scope->serializations && !--$this->scope->toserialize) {
             $this->scope = null;
         }
 
-        return $ret;
+        return [json_encode($ret)];
     }
-}
\ No newline at end of file
+
+    public function __unserialize($data)
+    {
+        if (is_array($data) && count($data) === 1 && isset($data[0])) {
+            $data = $data[0];
+        }
+
+        parent::__unserialize(json_decode($data, true));
+    }
+
+    public function unserialize($data)
+    {
+        $json = \unserialize($data);
+
+        $this->__unserialize($json);
+    }
+}
diff --git a/tests/SignedClosureTest.php b/tests/SignedClosureTest.php
index 6da82d4..b12e988 100644
--- a/tests/SignedClosureTest.php
+++ b/tests/SignedClosureTest.php
@@ -7,7 +7,6 @@
 
 namespace Opis\Closure\Test;
 
-use Opis\Closure\SecurityException;
 use Opis\Closure\SerializableClosure;
 
 class SignedClosureTest extends ClosureTest
@@ -102,6 +101,57 @@ class SignedClosureTest extends ClosureTest
         $this->assertTrue($closure());
     }
 
+    public function testSecurityProviderPersistsAfterNestedSerialization()
+    {
+        SerializableClosure::setSecretKey('secret');
+
+        $a = array();
+        $x = null;
+        $b = function() use(&$x){
+            return $x;
+        };
+        $c = function($i) use (&$a) {
+            $f = $a[$i];
+            return $f();
+        };
+        $a[] = $b;
+        $a[] = $c;
+        $x = $c;
+
+        $secProvider = SerializableClosure::getSecurityProvider();
+        $this->s($c);
+        $this->assertSame($secProvider, SerializableClosure::getSecurityProvider());
+    }
+
+    public function testSecurityProviderPersistsAfterFailedNestedSerialization()
+    {
+        SerializableClosure::setSecretKey('secret');
+
+        $a = array();
+        $x = null;
+        $b = function() use(&$x){
+            return $x;
+        };
+        $c = function($i) use (&$a) {
+            $f = $a[$i];
+            return $f();
+        };
+        $a[] = $b;
+        $a[] = $c;
+        $x = $c;
+
+        $secProvider = SerializableClosure::getSecurityProvider();
+        $value = serialize(new SerializableClosure($c));
+        $value = str_replace('$a[$i]', '$a[$z]', $value);
+        try {
+            $u = unserialize($value);
+        } catch (\Exception $e) {
+            $caught = true;
+        }
+        $this->assertTrue(isset($caught));
+        $this->assertSame($secProvider, SerializableClosure::getSecurityProvider());
+    }
+
     public function testJsonSecuredClosureWithoutSecuriyProvider()
     {
         SerializableClosure::setSecretKey('secret');
@@ -130,7 +180,7 @@ class SignedClosureTest extends ClosureTest
         };
 
         $value = serialize(new SerializableClosure($closure));
-        $value = str_replace('.', ',', $value);
+        $value = str_replace('closure', 'cl0sure', $value);
         SerializableClosure::removeSecurityProvider();
         unserialize($value);
     }
@@ -149,7 +199,7 @@ class SignedClosureTest extends ClosureTest
         };
 
         $value = serialize(new JsonSerializableClosure($closure));
-        $value = str_replace('hash', 'hash1', $value);
+        $value = str_replace('hash', 'ha5h', $value);
         SerializableClosure::removeSecurityProvider();
         unserialize($value);
     }
@@ -157,7 +207,7 @@ class SignedClosureTest extends ClosureTest
     public function testMixedEncodings()
     {
         $a = iconv('utf-8', 'utf-16', "Düsseldorf");
-        $b = utf8_decode("Düsseldorf");
+        $b = mb_convert_encoding("Düsseldorf", 'ISO-8859-1');
 
         $closure = function() use($a, $b) {
             return [$a, $b];
@@ -172,4 +222,4 @@ class SignedClosureTest extends ClosureTest
         $this->assertEquals($a, $r[0]);
         $this->assertEquals($b, $r[1]);
     }
-}
\ No newline at end of file
+}
