Source: php-opis-closure
Section: php
Priority: optional
Maintainer: Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>
Uploaders: Robin Gustafsson <rgson@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-phpcomposer,
               phpab,
               phpunit <!nocheck>,
               pkg-php-tools,
Standards-Version: 4.7.0
Homepage: https://github.com/opis/closure
Vcs-Browser: https://salsa.debian.org/php-team/pear/php-opis-closure
Vcs-Git: https://salsa.debian.org/php-team/pear/php-opis-closure.git -b debian/latest
Rules-Requires-Root: no

Package: php-opis-closure
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Multi-Arch: foreign
Description: serializable closures (anonymous functions) for PHP
 PHP library that can be used to serialize closures (anonymous functions) and
 arbitrary objects. It aims to overcome PHP's limitations regarding closure
 serialization by providing a wrapper that will make all closures serializable.
 .
 Key features:
  * Serializes any closure and arbitrary objects
  * Doesn't use eval for closure serialization or unserialization
  * Handles all variables referenced in use(), recursive closures, static
    closures and magic constants
  * Automatically resolves all class names, function names and constant names
    used inside the closure
  * Track closure's residing source by using the #trackme directive
  * Simple and very fast parser
  * Proper exception handling for unserialized closures
  * Supports cryptographically signed closures
  * Provides a reflector for serialized closures
  * Automatically detects when the scope and/or bound object of a closure needs
    to be serialized in order for the closure to work after deserialization
